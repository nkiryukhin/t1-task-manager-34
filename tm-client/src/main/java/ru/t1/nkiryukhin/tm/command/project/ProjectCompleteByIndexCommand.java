package ru.t1.nkiryukhin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.dto.request.ProjectCompleteByIndexRequest;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Complete project by index";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-complete-by-index";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(getToken());
        request.setIndex(index);
        getProjectEndpoint().completeProjectByIndex(request);
    }

}
