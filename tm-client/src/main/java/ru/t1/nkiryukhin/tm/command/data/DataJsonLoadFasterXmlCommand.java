package ru.t1.nkiryukhin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.request.DataJsonLoadFasterXmlRequest;
import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.exception.AbstractException;

public final class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load data from json file";
    }

    @NotNull
    @Override
    public String getName() {
        return "data-load-json";
    }

    @SneakyThrows
    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA LOAD JSON]");
        @NotNull final DataJsonLoadFasterXmlRequest request = new DataJsonLoadFasterXmlRequest(getToken());
        getDomainEndpointClient().loadDataJsonFasterXml(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}