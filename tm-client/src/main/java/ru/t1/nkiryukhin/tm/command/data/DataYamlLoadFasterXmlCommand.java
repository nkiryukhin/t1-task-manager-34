package ru.t1.nkiryukhin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.request.DataYamlLoadFasterXmlRequest;
import ru.t1.nkiryukhin.tm.enumerated.Role;

public final class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load data from yaml file";
    }

    @NotNull
    @Override
    public String getName() {
        return "data-load-yaml";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD YAML]");
        @NotNull final DataYamlLoadFasterXmlRequest request = new DataYamlLoadFasterXmlRequest(getToken());
        getDomainEndpointClient().loadDataYamlFasterXml(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
