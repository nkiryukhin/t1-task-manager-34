package ru.t1.nkiryukhin.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IServiceLocator {

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

}
