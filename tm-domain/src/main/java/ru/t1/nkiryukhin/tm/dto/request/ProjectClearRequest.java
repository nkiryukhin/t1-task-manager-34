package ru.t1.nkiryukhin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class ProjectClearRequest extends AbstractUserRequest {

    public ProjectClearRequest(@Nullable String token) {
        super(token);
    }

}
