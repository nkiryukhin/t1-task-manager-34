package ru.t1.nkiryukhin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataXmlLoadJaxBRequest extends AbstractUserRequest {

    public DataXmlLoadJaxBRequest(@Nullable String token) {
        super(token);
    }

}
