package ru.t1.nkiryukhin.tm.exception.user;

public final class AccessDeniedException extends AbstractUserException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
