package ru.t1.nkiryukhin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataBinaryLoadRequest extends AbstractUserRequest {

    public DataBinaryLoadRequest(@Nullable final String token) {
        super(token);
    }

}
